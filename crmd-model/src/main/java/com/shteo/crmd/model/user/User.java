package com.shteo.crmd.model.user;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Akuma on 2016/11/1.
 */
public class User {

    private Long id;
    //昵称
    private String name;
    //账号
    private String account;
    //密码
    private String password;
    //余额
    private BigDecimal balance;

    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
