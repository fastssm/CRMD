package com.shteo.crmd.web.feign;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.order.Order;
import com.shteo.crmd.web.feign.fallback.FCGoodsFallBack;
import com.shteo.crmd.web.feign.fallback.FCOrderFallBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Akuma on 2016/10/26.
 */
@FeignClient(name="crmd-order-service",fallback = FCOrderFallBack.class)
public interface IFCOrder {

    /**
     * 添加订单
     * @return
     */
    @RequestMapping("/order/add")
    AKResult add(@RequestBody Order order);

}
