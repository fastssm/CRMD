package com.shteo.crmd.web.exception;

/**
 * Created by Akuma on 16/4/5.
 */
public class APIException extends RuntimeException {

    private static final long serialVersionUID = -6982873782514543386L;

    /**
     * 构造函数
     * @param errMsg
     * @param cause
     */
    public APIException(String errMsg, Throwable cause) {
        super(errMsg,cause);
    }

    /**
     * 构造函数
     * @param errMsg
     */
    public APIException(String errMsg) {
        super(errMsg);
    }

}
