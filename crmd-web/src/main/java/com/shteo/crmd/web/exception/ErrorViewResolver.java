package com.shteo.crmd.web.exception;

import com.shteo.crmd.common.source.AKResult;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Akuma on 16/9/21.
 */
@Configuration
@ControllerAdvice
public class ErrorViewResolver{


    /**
     * AJAX||HTTP 接口异常
     * @param e
     * @return JSON数据
     */
    @ExceptionHandler
    public @ResponseBody String handleAPIException(APIException e) {
        return AKResult.builder().fail().errMsg(e.getMessage()).build2();
    }

}
