package com.shteo.crmd.web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Akuma on 16/9/28.
 */
//开启断路器
@EnableCircuitBreaker
//开启服务注册与发现
@EnableDiscoveryClient
//开启Feign调用接口
@EnableFeignClients
@SpringBootApplication
public class CRMWebApp implements CommandLineRunner{

    public static void main(String[] args) throws Exception{
        SpringApplication app = new SpringApplication(CRMWebApp.class);
        app.run(args);
    }

    //程序首次启动调用
    public void run(String... strings) throws Exception {
        System.out.println("CRM Server Boot Successfully");
    }

}
