package com.shteo.crmd.web.feign.fallback;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.goods.Goods;
import com.shteo.crmd.web.feign.IFCGoods;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Akuma on 2016/10/16.
 */
@Component
public class FCGoodsFallBack implements IFCGoods {


    @Override
    public AKResult add(@RequestBody Goods goods) {
        return AKResult.builder().fail().errMsg("程序异常");
    }

    @Override
    public List<Goods> getAllGoods() {
        return null;
    }

    @Override
    public Goods getGoodsById(@RequestParam("goodsId") Long goodsId) {
        return null;
    }

    @Override
    public AKResult delGoodsById(@RequestParam("goodsId") Long goodsId) {
        return AKResult.builder().fail().errMsg("程序异常");
    }

    @Override
    public AKResult updateGoodsById(@RequestBody Goods goods) {
        return AKResult.builder().fail().errMsg("程序异常");
    }

    /**
     * 扣除库存
     * @param id
     * @param stock
     * @return
     */
    @Override
    public AKResult subStock(@RequestParam("id") Long id, @RequestParam("stock") Integer stock) {
        return AKResult.builder().fail().errMsg("程序异常");
    }
}
