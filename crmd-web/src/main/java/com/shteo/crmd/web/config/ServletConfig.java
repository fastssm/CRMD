package com.shteo.crmd.web.config;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Akuma on 2016/10/18.
 */
@Configuration
public class ServletConfig {

    @Bean
    public ServletRegistrationBean hystrixMetricsStreamServlet(){
        ServletRegistrationBean reg = new ServletRegistrationBean();
        reg.setServlet(new HystrixMetricsStreamServlet());
        reg.addUrlMappings("/hystrix.stream");
        return reg;
    }

}
