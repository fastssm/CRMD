package com.shteo.crmd.web.feign;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.goods.Goods;
import com.shteo.crmd.web.feign.fallback.FCGoodsFallBack;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Akuma on 2016/10/16.
 */
@FeignClient(name="crmd-goods-service",fallback = FCGoodsFallBack.class)
public interface IFCGoods {

    /**
     * 添加商品
     * @param goods 商品实体
     * @return
     */
    @RequestMapping(value = "/goods/add",method = RequestMethod.POST)
    AKResult add(@RequestBody Goods goods);

    /**
     * 查找所有商品
     * @return
     */
    @RequestMapping(value = "/goods/get",method = RequestMethod.GET)
    List<Goods> getAllGoods();

    /**
     * 查找商品
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping(value = "/goods/getById",method = RequestMethod.GET)
    Goods getGoodsById(@RequestParam("goodsId") Long goodsId);

    /**
     * 删除商品
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping(value = "/goods/delById",method = RequestMethod.DELETE)
    AKResult delGoodsById(@RequestParam("goodsId")Long goodsId);

    /**
     * 更新商品
     * @param goods
     * @return
     */
    @RequestMapping(value = "/goods/updateById",method = RequestMethod.PUT)
    AKResult updateGoodsById(@RequestBody Goods goods);

    /**
     * 扣除库存
     * @param id
     * @param stock
     * @return
     */
    @RequestMapping(value = "/subStock",method = RequestMethod.PUT)
    AKResult subStock(@RequestParam("id")Long id,@RequestParam("stock")Integer stock);

}
