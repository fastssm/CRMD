package com.shteo.crmd.web.feign.fallback;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.order.Order;
import com.shteo.crmd.web.feign.IFCOrder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by Akuma on 2016/10/26.
 */
@Component
public class FCOrderFallBack implements IFCOrder {


    @Override
    public AKResult add(@RequestBody Order order) {
        return AKResult.builder().fail().errMsg("服务异常");
    }
}
