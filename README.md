#Spring Cloud Project

#架构
Spring Cloud + Spring Boot + Sharding-jdbc + Maven多模块管理

#MAVEN模块
crmd-server           : (8761)服务注册中心
crmd-gateway          : (8080)服务网关(验证，负载请求)
crmd-web              : (8081)API接口
crmd-goods-service    : (8082)商品模块（集成当当网Sharding-jdbc）
crmd-order-service    : (8083)订单模块（集成当当网Sharding-jdbc）
crmd-user-service     : (8084)用户模块（集成当当网Sharding-jdbc）
crmd-model            : 所有模块的实体
crmd-common           : 公共模块（工具类,资源......）
crmd-config           : 配置中心(项目配置集中处理，方便管理)

#数据库信息(分库分表,动态扩容待解决)
1：商品模块
    库：crmd_goods_{1~2}
    表：goods_{0~1}
    sql:
    CREATE TABLE `goods_{0~1}` (
      `id` bigint(20) NOT NULL COMMENT '商品ID',
      `goods_name` varchar(255) NOT NULL COMMENT '商品名称',
      `stock` int(11) NOT NULL COMMENT '商品库存',
      `create_time` datetime NOT NULL COMMENT '创建时间',
      `price` decimal(11,2) NOT NULL COMMENT '商品价格',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';
    
2：订单模块
    库：crmd_order_{1~2}
    表：order_{0~1}
    sql:
    CREATE TABLE `order_{0~1}` (
      `id` bigint(20) NOT NULL,
      `goods_id` bigint(20) NOT NULL COMMENT '商品ID',
      `quantity` int(11) NOT NULL COMMENT '商品数量',
      `total_money` decimal(11,2) NOT NULL COMMENT '订单总金额',
      `create_time` datetime NOT NULL COMMENT '创建时间',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';
    
分库分表策略：
    分库：`(ID/100)%2`
    分表：`(ID/10)%2`

`数据库sql文件在附件中的database.zip文件中.`

    
#目前架构图(待完善)
![](http://chuantu.biz/t5/38/1476863611x1961023832.png)

#服务中心
两台服务服务注册中心(相互注册)
--ip239 192.168.1.239:8761
--ip251 192.168.1.251:8761

#熔断监控视图
运行附件里的standalone-hystrix-dashboard-1.5.6-all.jar包,打开监控页面：http://localhost:7979/hystrix-dashboard，
输入监控的参数地址：http://localhost:8080/hystrix.stream
点击Add Stream然后点击Monitor Streams:
![](http://chuantu.biz/t5/38/1476763679x1961023832.png)


#API文档视图（Swagger2）
功能：可以查看，调试暴露的API接口
地址：http://localhost:8080/swagger-ui.html
![](http://chuantu.biz/t5/38/1476674694x1961023832.png)

#JWT验证(初略版,待完善)
1：先请求登陆地址：http://localhost:8080/api/test/login 参数：account:admin  password:admin
获取通行凭证token：
{"data":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJBa3VtYSIsImF1ZCI6IjEwMDAwIiwiZXhwIjoxNDc3MjkxNDU0LCJpYXQiOjE0NzcyODQyNTR9.1h1q6ZYHOZIMjIXP28s8kiptn8XuJMzu0memcgZ2vR4","errCode":"00","errMsg":"登陆成功"}
2：除了登陆接口，其他的接口请求头部都加上Authorization ，值为"Bearer [刚获取的token]"


#测试分布式事务(未完成)
模拟业务场景：用户下单，订单模块添加一条数据，同时修改商品模块中商品的购买数量。


#最近在用Spring cloud开发公司项目，此Demo暂时不会更新了.