package com.shteo.crmd.server;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by Akuma on 16/9/28.
 */
@EnableEurekaServer
@SpringBootApplication
public class CRMDServerApplication implements CommandLineRunner{

    public static void main(String[] args) throws Exception{
        new SpringApplicationBuilder(CRMDServerApplication.class).web(true).run(args);
    }

    //程序首次启动调用
    public void run(String... strings) throws Exception {
        System.out.println("CRMD Server Boot Successfully");
    }


}
