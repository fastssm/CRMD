package com.shteo.crmd.goods;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by Akuma on 16/9/28.
 */
//开启服务注册与发现
@EnableDiscoveryClient
@SpringBootApplication
public class CRMDGoodsApplication implements CommandLineRunner{

    //日志
    private Logger logger = Logger.getLogger(getClass());

    public static void main(String[] args) throws Exception{
        SpringApplication app = new SpringApplication(CRMDGoodsApplication.class);
        app.run(args);
    }

    //程序首次启动调用
    public void run(String... strings) throws Exception {
        logger.info("CRMD Goods Micro-Service Server Boot Successfully");
    }
}
