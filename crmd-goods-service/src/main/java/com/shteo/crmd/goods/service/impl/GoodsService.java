package com.shteo.crmd.goods.service.impl;

import com.shteo.crmd.common.helper.SnowFlakeIDHelper;
import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.goods.dao.IGoodsDao;
import com.shteo.crmd.goods.service.IGoodsService;
import com.shteo.crmd.model.goods.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Akuma on 2016/10/16.
 */
@Service
@Transactional
public class GoodsService implements IGoodsService {

    @Autowired
    private SnowFlakeIDHelper snowFlakeIDHelper;

    @Autowired
    private IGoodsDao goodsDao;

    @Override
    public AKResult addGoods(Goods goods) {
        goods.setId(snowFlakeIDHelper.nextId());
        goods.setCreateTime(new Date());
        goodsDao.addGoods(goods);
        return AKResult.builder().ok().errMsg("添加商品成功");
    }

    @Override
    public List<Goods> getAllGoods() {
        return goodsDao.getAllGoods();
    }

    @Override
    public Goods getGoodsById(Long goodsId) {
        return goodsDao.getGoodsById(goodsId);
    }

    @Override
    public AKResult delGoodsById(Long goodsId) {
        if (goodsDao.delGoodsById(goodsId)>0){
            return AKResult.builder().ok().errMsg("删除成功");
        }
        return AKResult.builder().fail().errMsg("删除失败");
    }

    @Override
    public AKResult updateGoodsById(Goods goods) {
        if (goodsDao.updateGoodsById(goods)>0){
            return AKResult.builder().ok().errMsg("更新成功");
        }
        return AKResult.builder().fail().errMsg("更新失败");
    }

    /**
     * 扣除库存
     *
     * @param map
     * @return
     */
    @Override
    public AKResult subGoodsStock(Map<String, Object> map) {
        if (goodsDao.subGoodsStock(map)>0){
            return AKResult.builder().ok().errMsg("扣除成功");
        }
        return AKResult.builder().fail().errMsg("扣除失败");
    }
}
