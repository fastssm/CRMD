package com.shteo.crmd.goods.controller;

import com.google.common.collect.Maps;
import com.shteo.crmd.goods.service.IGoodsService;
import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.goods.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Akuma on 2016/10/16.
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private IGoodsService goodsService;

    /**
     * 添加商品
     * @param goods
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public AKResult add(@RequestBody Goods goods){
        return goodsService.addGoods(goods);
    }

    /**
     * 查询商品集合
     * @return
     */
    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public List<Goods> get(){
        return goodsService.getAllGoods();
    }

    /**
     * 查询商品明细
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping(value = "/getById",method = RequestMethod.GET)
    public Goods getGoodsById(@RequestParam("goodsId")Long goodsId){
        return goodsService.getGoodsById(goodsId);
    }

    /**
     * 删除商品
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping(value = "/delById",method = RequestMethod.DELETE)
    public AKResult delGoodsById(@RequestParam("goodsId")Long goodsId){
        return goodsService.delGoodsById(goodsId);
    }

    /**
     * 更新商品
     * @param goods
     * @return
     */
    @RequestMapping(value = "/updateById",method = RequestMethod.PUT)
    public AKResult updateGoodsById(@RequestBody Goods goods){
        return goodsService.updateGoodsById(goods);
    }

    /**
     * 扣除库存
     * @param id
     * @param stock
     * @return
     */
    @RequestMapping(value = "/subStock",method = RequestMethod.PUT)
    public AKResult subStock(@RequestParam("id")Long id,@RequestParam("stock")Integer stock){
        Map<String,Object> map = Maps.newHashMap();
        map.put("id",id);
        map.put("stock",stock);
        return goodsService.subGoodsStock(map);
    }
}
