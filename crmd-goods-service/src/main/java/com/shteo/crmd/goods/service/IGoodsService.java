package com.shteo.crmd.goods.service;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.goods.Goods;

import java.util.List;
import java.util.Map;

/**
 * Created by Akuma on 2016/10/16.
 */
public interface IGoodsService {

    /**
     * 添加商品
     * @param goods
     */
    AKResult addGoods(Goods goods);

    /**
     * 查询所有商品
     * @return
     */
    List<Goods> getAllGoods();

    /**
     * 查询商品
     * @param goodsId 商品ID
     * @return
     */
    Goods getGoodsById(Long goodsId);

    /**
     * 删除商品
     * @param goodsId 商品ID
     * @return
     */
    AKResult delGoodsById(Long goodsId);

    /**
     * 更新商品
     * @param goods
     * @return
     */
    AKResult updateGoodsById(Goods goods);

    /**
     * 扣除库存
     * @param map
     * @return
     */
    AKResult subGoodsStock(Map<String,Object> map);

}
