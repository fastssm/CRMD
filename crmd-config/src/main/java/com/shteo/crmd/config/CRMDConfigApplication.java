package com.shteo.crmd.config;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * Created by Akuma on 16/9/29.
 */
@EnableConfigServer
@EnableDiscoveryClient
@SpringBootApplication
public class CRMDConfigApplication implements CommandLineRunner{

    //日志
    private Logger logger = Logger.getLogger(getClass());

    public static void main(String[] args) throws Exception{
        SpringApplication app = new SpringApplication(CRMDConfigApplication.class);
        app.run(args);
    }

    //程序首次启动调用
    public void run(String... strings) throws Exception {
        logger.info("CRMD Config Server Boot Successfully");
    }

}
