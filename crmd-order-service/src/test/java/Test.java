import com.shteo.crmd.common.helper.SnowFlakeIDHelper;
import com.shteo.crmd.wechat.Application;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by Akuma on 2016/10/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class) // 指定我们SpringBoot工程的Application启动类
public class Test {

    @Resource
    private SnowFlakeIDHelper snowFlakeIDHelper;

    @org.junit.Test
    public void nextId() throws Exception {
        System.out.println(snowFlakeIDHelper.nextId());
    }
}
