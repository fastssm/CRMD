package com.shteo.crmd.order.dao;

import com.shteo.crmd.model.order.Order;

/**
 * Created by Akuma on 2016/10/26.
 */
public interface IOrderDao {

    /**
     * 添加订单
     * @param order
     */
    void addOrder(Order order);

}
