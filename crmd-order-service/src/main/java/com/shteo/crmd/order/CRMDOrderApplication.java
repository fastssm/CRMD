package com.shteo.crmd.order;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by Akuma on 16/9/28.
 */

@SpringBootApplication
@EnableDiscoveryClient
public class CRMDOrderApplication implements CommandLineRunner{

    //日志
    private Logger logger = Logger.getLogger(getClass());

    public static void main(String[] args) throws Exception{
        new SpringApplicationBuilder(CRMDOrderApplication.class).web(true).run(args);
    }

    //程序首次启动调用
    public void run(String... strings) throws Exception {
        logger.info("CRMD Order Micro-Service Server Boot Successfully");
    }
}
