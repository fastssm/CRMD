package com.shteo.crmd.order.service.impl;

import com.shteo.crmd.common.helper.SnowFlakeIDHelper;
import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.order.Order;
import com.shteo.crmd.order.dao.IOrderDao;
import com.shteo.crmd.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Akuma on 2016/10/26.
 */
@Service
@Transactional
public class OrderService implements IOrderService {

    @Autowired
    private IOrderDao orderDao;

    @Autowired
    private SnowFlakeIDHelper snowFlakeIDHelper;

    @Override
    public AKResult addOrder(Order order) {
        order.setId(snowFlakeIDHelper.nextId());
        order.setCreateTime(new Date());
        orderDao.addOrder(order);
        return AKResult.builder().ok().errMsg("添加成功");
    }
}
