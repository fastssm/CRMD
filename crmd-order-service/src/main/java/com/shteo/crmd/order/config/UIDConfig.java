package com.shteo.crmd.order.config;

import com.shteo.crmd.common.helper.SnowFlakeIDHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Akuma on 16/10/7.
 */
@Configuration
public class UIDConfig {

    @Value("${sfid.worker.id:1}")
    private Long workerId;

    @Value("${sfid.datacenter.id:0}")
    private Long dataCenterId;

    @Bean
    public SnowFlakeIDHelper snowFlakeID(){
        return new SnowFlakeIDHelper(workerId,dataCenterId);
    }

}
