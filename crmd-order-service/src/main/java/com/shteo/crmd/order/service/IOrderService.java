package com.shteo.crmd.order.service;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.order.Order;

/**
 * Created by Akuma on 2016/10/26.
 */
public interface IOrderService {

    /**
     * 添加订单
     * @param order
     */
    AKResult addOrder(Order order);

}
