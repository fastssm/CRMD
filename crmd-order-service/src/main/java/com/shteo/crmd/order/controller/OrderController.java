package com.shteo.crmd.order.controller;

import com.shteo.crmd.common.source.AKResult;
import com.shteo.crmd.model.order.Order;
import com.shteo.crmd.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Akuma on 2016/10/26.
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    /**
     * 添加订单
     * @return
     */
    @RequestMapping("/add")
    public AKResult add(@RequestBody Order order){
        return orderService.addOrder(order);
    }



}
