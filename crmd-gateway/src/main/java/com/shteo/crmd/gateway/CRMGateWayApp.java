package com.shteo.crmd.gateway;

import com.shteo.crmd.gateway.filter.AccessFilter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Akuma on 16/9/28.
 */
@EnableZuulProxy
@SpringCloudApplication
public class CRMGateWayApp implements CommandLineRunner{

    public static void main(String[] args) throws Exception{
        SpringApplication app = new SpringApplication(CRMGateWayApp.class);
        app.run(args);
    }

    //程序首次启动调用
    public void run(String... strings) throws Exception {
        System.out.println("CRM GateWay Boot Successfully");
    }

    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();
    }


}
