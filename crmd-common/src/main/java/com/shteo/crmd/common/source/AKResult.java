package com.shteo.crmd.common.source;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.shteo.crmd.common.helper.StringHelper;

public class AKResult {

	/**
	 * 00 代表成功
	 * 01 代表失败
	 */
	private String errCode; //失败时的失败码（只有在失败的时候才会设置）
	
	private String errMsg; //错误原因
	
	private Object data; //附加数据


	private AKResult(){}

	public static AKResult builder(){
		return new AKResult();
	}

	/**
	 * 设置返回码
	 * @return
	 */
	public AKResult ok(){
		this.errCode = AKConstants.ERR_OK;
		return this;
	}

	/**
	 * 设置返回码
	 * @return
	 */
	public AKResult fail(){
		this.errCode = AKConstants.ERR_FAIL;
		return this;
	}

	/**
	 * 设置返回码
	 * @param errCode
	 * @return
	 */
	public AKResult fail(String errCode){
		this.errCode = errCode;
		return this;
	}


	/**
	 * 设置返回结果
	 * @param errMsg
	 * @return
	 */
	public AKResult errMsg(String errMsg){
		this.errMsg = errMsg;
		return this;
	}

	/**
	 * 设置返回数据
	 * @return
	 */
	public AKResult data(Object data){
		this.data = data;
		return this;
	}

	/**
	 * 构建返回值 (ID不会过滤)
	 * @return
	 */
	public String build2(){
		if (Strings.isNullOrEmpty(this.errCode)){
			this.errCode = AKConstants.ERR_FAIL;
		}
		if (Strings.isNullOrEmpty(this.errMsg)){
			if (this.errCode == AKConstants.ERR_OK){
				this.errMsg = AKConstants.ERR_OK_MSG;
			}
			if (this.errCode == AKConstants.ERR_FAIL){
				this.errMsg = AKConstants.ERR_FAIL_MSG;
			}
		}
		if (this.data == null){
			this.data = new JSONObject();
		}
		return JSONObject.toJSONString(this);
	}


	/**
	 * 构建返回值
	 * @return
	 */
	public String build(){
		if (Strings.isNullOrEmpty(this.errCode)){
			this.errCode = AKConstants.ERR_FAIL;
		}
		if (Strings.isNullOrEmpty(this.errMsg)){
			if (this.errCode == AKConstants.ERR_OK){
				this.errMsg = AKConstants.ERR_OK_MSG;
			}
			if (this.errCode == AKConstants.ERR_FAIL){
				this.errMsg = AKConstants.ERR_FAIL_MSG;
			}
		}
		if (this.data == null){
			this.data = new JSONObject();
		}
		return StringHelper.toJSONString(this);
	}



	public String getErrCode() {
		return errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public Object getData() {
		return data;
	}

	public boolean success(){
		return this.errCode!=null&&this.errCode.equals("00");
	}
}
