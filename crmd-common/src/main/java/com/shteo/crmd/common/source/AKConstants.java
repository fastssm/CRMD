package com.shteo.crmd.common.source;

/**
 * Created by Akuma on 15/9/14.
 */
public class AKConstants {

    /**
     * 超级管理员代码
     */
    public static final String SUPER_ADMIN_CODE = "BX14554180230091534";

    /**
     * 每页10条
     */
    public static final int PAGE_SIZE = 10;

    /**
     * 本地IP地址
     */
    public static final String LOCAL_HOST = "127.0.0.1";

    /**
     * 初始化密码
     */
    public static final String INIT_PASS_WORD = "000000";

    /**
     * 正常
     */
    public static final String STATUS_OK = "1";

    /**
     * 异常
     */
    public static final String STATUS_EX = "0";

    /**
     * success字符串
     */
    public static final String SUCCESS = "success";

    /**
     * 消息关键字
     */
    public final static String MSG_KEY = "message";

    /**
     * 请求成功通用码
     */
    public final static String ERR_OK = "00";

    /**
     * 请求失败通用码
     */
    public final static String ERR_FAIL = "01";

    /**
     * 请求成功通用返回信息
     */
    public final static String ERR_OK_MSG = "执行成功";

    /**
     * 请求失败通用返回信息
     */
    public final static String ERR_FAIL_MSG = "系统繁忙";

    /**
     * 空字符串
     */
    public final static String EMPTY_STRING = "";

    /**
     * redis中存储token的key
     */
    public final static String ACCESS_TOKEN_KEY = "wx.token.";

    /**
     * 多客服消息格式
     */
    public static final String KF_XML = "<xml><ToUserName><![CDATA[FROM_USER_NAME]]></ToUserName><FromUserName><![CDATA[TO_USER_NAME]]></FromUserName><CreateTime>CREATE_TIME</CreateTime><MsgType><![CDATA[transfer_customer_service]]></MsgType></xml>";

    /**
     * REDIS中session的key
     */
    public static final String REDIS_SESSION_KEY = "spring:session:sessions:SESSIONID";
    public static final String REDIS_SESSION_EXPIRES_KEY = "spring:session:sessions:expires:SESSIONID";

    /**
     * REDIS中service的key
     * service:学校id:学科:服务类型（1：错题本：2：提分方案：3：提分宝）:学期:年级:订单类型（0：正式；1：试用）
     */
    public static final String REDIS_SERVICES_KEY = "services:SCHOOL:SUBJECT:CLASSIFY:SEMESTER:TYPE";

    /**
     * REDIS中service的key
     * service:trial:服务ID:班级ID
     */
    public static final String REDIS_TRIAL_SERVICE_KEY = "service:trial:ZSY_SERVICE_ID:CLASS_ID";

    /**
     * REDIS中service的key
     * service:official:服务ID:学生ID
     */
    public static final String REDIS_OFFICIAL_SERVICE_KEY = "service:official:ZSY_SERVICE_ID:STUDENT_ID";

    /**
     * REDIS中service的明细
     */
    public static final String REDIS_SERVICE_KEY = "service:ZSY_SERVICE_ID";

    /**
     * 业务库数据库配置信息
     */
    public static final String REDIS_BUSINESS_DATABASE_KEY = "spider:database:SCHOOLID";

    /**
     * 子库连接jdbc url
     */
    public static final String SLAVE_DATABASE_URL = "jdbc:mysql://HOST/DATABASE?useUnicode=true&characterEncoding=UTF-8";


    /************************URL部分**************************/
    /**
     * 获取token的url限200（次/天）
     */
    public final static String GET_ACCESS_TOKEN = "https://feign.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

    /**
     * 获取openid
     */
    public final static String GET_OPENID = "https://feign.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

    /**
     * 获取用户基本信息
     */
    public final static String GET_USER_INFO = "https://feign.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

    /**
     * 发送模板消息
     */
    public static final String SEND_TEMPLATE_MSG = "https://feign.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

    /**
     * 创建微信菜单
     */
    public static final String CREATE_WX_MENU = "https://feign.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";


    /**
     * 新增其他类型永久素材
     */
    public static final String ADD_MATERIAL = "https://feign.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE";

    /**
     * 上传媒体文件
     */
    public static final String UPLOAD_MEDIA = "http://file.feign.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";


    /**
     * 上传图文消息素材(群发)
     */
    public static final String UPLOADNEWS = "https://feign.weixin.qq.com/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN";


    /**
     * 根据标签进行群发
     */
    public static final String MASS_SENDALL = "https://feign.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN";

    /**
     * 根据OpenID列表群发
     */
    public static final String MASS_SEND = "https://feign.weixin.qq.com/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN";


    ///////////////////////
    // 群发消息的消息类型
    ///////////////////////
    public static final String MASS_MSG_NEWS = "mpnews";
    public static final String MASS_MSG_TEXT = "text";
    public static final String MASS_MSG_VOICE = "voice";
    public static final String MASS_MSG_IMAGE = "image";
    public static final String MASS_MSG_VIDEO = "mpvideo";

    ///////////////////////
    // 上传多媒体文件的类型
    ///////////////////////
    public static final String MEDIA_IMAGE = "image";
    public static final String MEDIA_VOICE = "voice";
    public static final String MEDIA_VIDEO = "video";
    public static final String MEDIA_THUMB = "thumb";
    public static final String MEDIA_FILE = "file";


    /**
     * 商品类目被使用
     */
    public static final Integer GOODS_TYPE_UQ = 20001;
    /**
     * 商品类目名称已存在
     */
    public static final Integer GOODS_TYPENAME_UQ = 20002;
    /**
     * 商品名称已存在
     */
    public static final Integer GOODS_NAME_UQ = 20003;

    /**
     * 学校已存在
     */
    public static final Integer SCHOOL_UQ = 20004;

    /**
     * 助教已存在
     */
    public static final Integer COACH_UQ = 20005;



}
